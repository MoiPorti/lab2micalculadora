# SENCILLA CALCULADORA PARA LA ASIGNATURA DE INGENIERIA DEL SOFTWARE AVANZADA
import sys
# Función suma se necesitan 2 números para realizar la operación
def suma(x, y):
    return x + y

# Función resta se necesitan 2 números para realizar la operación
def resta(x, y):
    return x - y

# Función multiplicación se necesitan 2 números para realizar la operación
def multiplicacion(x, y):
    return x * y

# Función división se necesitan 2 números para realizar la operación
def division(x, y):
    return x / y

# Esta función hace la raíz cuadrada del número que el usuario introduzca
def raiz(x):

    return x
    
print("SELECCIONA UNA OPCION")
print("1.SUMA")
print("2.RESTA")
print("3.MULTIPLICACION")
print("4.DIVISION")
print("5.RAIZ CUADRADA")

while True:
    # El usuario debe pulsar unas de las siguientes teclas, si no 
    choice = input("ELIJE UNA OPCION(1/2/3/4/5 -> PARA SALIR PULSE 3 CEROS): ")

    # Check if choice is one of the four options
    if choice in ('0', '1', '2', '3', '4', '5'):
      
        num1 = float(input("INTRODUCE UN NUMERO: "))
         
        num2 = float(input("INTRODUCE OTRO NUMERO: "))

        if choice == '1':
            print(num1, "+", num2, "=", suma(num1, num2))

        elif choice == '2':
            print(num1, "-", num2, "=", resta(num1, num2))

        elif choice == '3':
            print(num1, "*", num2, "=", multiplicacion(num1, num2))

        elif choice == '4':
            print(num1, "/", num2, "=", division(num1, num2))

        elif choice == '5':
            print("la raiz cuadrada de", num1, "es", (num1**.5))
            
        elif choice == '0':

                sys.exit()
    else:
        print("ERROR FATAL!! POR FAVOR PULSA UN NUMERO CORRECTO 1-5")

        sys.exit() 
       


